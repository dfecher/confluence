# Polarity Confluence Integration

The Polarity Confluence integration allows Polarity to search Confluence to return any Space, Page of Blog that is contained in confluence.

![image](https://user-images.githubusercontent.com/22529325/41306906-a05121f4-6e45-11e8-8ccb-e4ab156826f6.png)


## Confluence Integration Options

### Confluence URL

URL used to access your instance of Confluence.

### Confluence UserName

Username used for individual to access Confluence.

### API Key

Confluence API Key, can be generated from a users profile page in Confluence.

https://id.atlassian.com/manage/api-tokens


## Installation Instructions

Installation instructions for integrations are provided on the [PolarityIO GitHub Page](https://polarityio.github.io/).

## Polarity

Polarity is a memory-augmentation platform that improves and accelerates analyst decision making.  For more information about the Polarity platform please see:

https://polarity.io/
