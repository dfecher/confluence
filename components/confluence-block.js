'use strict';

polarity.export = PolarityComponent.extend({
  details: Ember.computed.alias('block.data.details'),

  showPgDetails: Ember.computed('details.length', function() {
    const detailsLength = this.get('details.length');
    const viewState = Ember.A();
    for (let i = 0; i < detailsLength; i++) {
      viewState.push(false);
    }
    return viewState;
  }),
  showPgBody: Ember.computed('details.length', function() {
    const detailsLength = this.get('details.length');
    const viewState = Ember.A();
    for (let i = 0; i < detailsLength; i++) {
      viewState.push(false);
    }
    return viewState;
  }),
  showBgDetails: Ember.computed('details.length', function() {
    const detailsLength = this.get('details.length');
    const viewState = Ember.A();
    for (let i = 0; i < detailsLength; i++) {
      viewState.push(false);
    }
    return viewState;
  }),
  showBgBody: Ember.computed('details.length', function() {
    const detailsLength = this.get('details.length');
    const viewState = Ember.A();
    for (let i = 0; i < detailsLength; i++) {
      viewState.push(false);
    }
    return viewState;
  }),
  //Toggle Details and Body
  actions: {
    togglePgVisibility(index) {
      this.toggleProperty('showPgDetails.' + index);
    },
    togglePgBody(index) {
      this.toggleProperty('showPgBody.' + index);
    },
    toggleBgVisibility(index) {
      this.toggleProperty('showBgDetails.' + index);
    },
    toggleBgBody(index) {
      this.toggleProperty('showBgBody.' + index);
    }
  }

});